require 'byebug'
# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  missing_nums = []
  nums.each_with_index do |num, idx|
    count = num
    if nums[idx + 1] != num + 1 && !nums[idx + 1].nil?
      while count < nums[idx + 1] - 1
        count += 1
        missing_nums << count
      end
    end
  end

  missing_nums
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  binary_length = binary.length - 1
  result = 0
  binary.each_char do |ch|
    result += ch.to_i * 2**binary_length
    binary_length -= 1
  end
  result
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    selected = []
    self.map { |el| selected << el if prc.call(el) }
    selected.to_h
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    merged = []
    new_hsh = hash
    if block_given?
      hash.map do |k, v|
        merged << [k, self[k], v] if self.include?(k)
      end

      merged.each_with_index do |el, idx|
        if el.length >= 3
          new_hsh[el.first] = yield(el)
        end
      end
      new_hsh

    else
      hash.each { |k, v| self[k] = v }
      return self
    end
  end
end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  lucas = [2, 1]
  if n >= 1
    (0..n).each do |idx|
      lucas << lucas[idx - 2] + lucas[idx - 1] if idx > 1
    end
    return lucas.last
  else
    n.abs.times { lucas.unshift(lucas[1] - lucas[0]) }
    return lucas.first
  end
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  reversed = string.reverse
  n = string.length - 1
  longest = []

  string.each_char.with_index do |ch1, idx1|
    reversed.each_char.with_index do |ch2, idx2|
      if ch1 == ch2
        if string[idx1..(n - idx2)] == string[idx1..(n - idx2)].reverse
          longest << string[idx1..(n - idx2)].length
        end
      end
    end
  end

  return false if longest.max <= 2
  longest.max
end
